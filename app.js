const SHA256 = require('crypto-js/sha256');

class Transaction {
    constructor(fromAddress, toAddress, amount) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
    }
}

class Block {
    constructor(timestamp, transactions, previousHash = '') {
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        // using SHA256 to generate hash
        return SHA256(this.timestamp + this.previousHash + JSON.stringify(this.transactions) + this.nonce).toString();

    }

    mineNewBlock(difficulty) {
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')) {
            this.nonce ++;
            this.hash = this.calculateHash()
        }
        console.log('A new block was mined with hash ' + this.hash)
    }
}


class Blockchain {
    constructor() {
        // first variable is the first genesis block
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 2;
        this.pendingTransactions = [];
        this.miningReward = 10;
    }

    createGenesisBlock() {
        return new Block("01/01/2019", "this is the genesis block", "0")
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    minePendingTransactions(miningRewardAddress) {
        let block = new Block(Date.now(), this.pendingTransactions, this.getLatestBlock().hash);
        block.mineNewBlock(this.difficulty);
        console.log('Block mined successfully');

        this.chain.push(block);
        this.pendingTransactions = [
            new Transaction(null, miningRewardAddress, this.miningReward)
        ];
    }

    createTransaction(transaction) {
        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAddress(address) {
        let balance = 0;

        for(const block of this.chain) {
            for(const trans of block.transactions) {

                if(trans.fromAddress === address) {
                    balance = balance - trans.amount;
                }
                if(trans.toAddress === address) {
                    balance = balance + trans.amount;
                }
            }
        }
        return balance;
    }

    checkBlockChainValid() {
        // without the genesis block
        for(let i=1; this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i-1];

            if(currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if(currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }

            return true;
         }
    }
}

// creating 2 blocks
let bittyCoin = new Blockchain();

let transaction1 = new Transaction('tom', "jerry", 100);
bittyCoin.createTransaction(transaction1);

let transaction2 = new Transaction('jerry', "tom", 30);
bittyCoin.createTransaction(transaction2);

console.log('started mining by the miner...')
bittyCoin.minePendingTransactions("donald");


console.log("Balance for tom is " + bittyCoin.getBalanceOfAddress("tom"));
console.log("Balance for jerry is " + bittyCoin.getBalanceOfAddress("jerry"));
console.log("Balance for donald is " + bittyCoin.getBalanceOfAddress("donald"));

let transaction3 = new Transaction('tom', "jerry", 100);
bittyCoin.createTransaction(transaction3);

let transaction4 = new Transaction('jerry', "tom", 30);
bittyCoin.createTransaction(transaction4);

console.log('started mining by the miner...')
bittyCoin.minePendingTransactions("donald");

console.log("Balance for tom is " + bittyCoin.getBalanceOfAddress("tom"));
console.log("Balance for jerry is " + bittyCoin.getBalanceOfAddress("jerry"));
console.log("Balance for donald is " + bittyCoin.getBalanceOfAddress("donald"));
